package com.roshless.gitstats;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Error {
    @JsonProperty(value = "error_message")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Error(String message) {
        this.message = message;
    }
}
