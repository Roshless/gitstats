package com.roshless.gitstats;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GitRepoStats {
    @JsonProperty(value = "repo_name")
    private String repoName;
    @JsonProperty(value = "all_commits")
    private int allCommits;
    private int contributors;

    public GitRepoStats(String repoName, int allCommits, int contributors) {
        this.repoName = repoName;
        this.allCommits = allCommits;
        this.contributors = contributors;
    }

    public String getRepoName() {
        return repoName;
    }

    public void setRepoName(String repoName) {
        this.repoName = repoName;
    }

    public int getAllCommits() {
        return allCommits;
    }

    public void setAllCommits(int allCommits) {
        this.allCommits = allCommits;
    }

    public int getContributors() {
        return contributors;
    }

    public void setContributors(int contributors) {
        this.contributors = contributors;
    }
}
